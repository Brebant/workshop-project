var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'C://WorkshopApp/Recu/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname) // Appending .pdf
  }
})

var upload = multer({ storage: storage });

// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');

  // authorized headers for preflight requests
  // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();

  app.options('*', (req, res) => {
      // allowed XHR methods
      res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
      res.send();
  });
});

app.get('/bills', function (req, res) {
  var sql = require("mssql");

  // config for your database
  var config = {
      user: 'workUser',
      password: 'azert20.',
      server: 'localhost',
      database: 'Workshop'
  };

  // connect to your database
  sql.connect(config, function (err) {

      if (err) console.log(err);

      // create Request object
      var request = new sql.Request();

      // query to the database and get the records
      //request.query('select * from Factures', function (err, recordset) {
      request.query(`DECLARE @PageNumber AS INT\
                     DECLARE @RowsOfPage AS INT\
                     SET @PageNumber=${req.query.page}\
                     SET @RowsOfPage=10\
                     SELECT * FROM Factures\
                     ORDER BY ${req.query.sort} ${req.query.order}\
                     OFFSET (@PageNumber-1)*@RowsOfPage ROWS\
                     FETCH NEXT @RowsOfPage ROWS ONLY`, function (err, bills) {

          if (err) console.log(err)

          // send records as a response
          request.query(`SELECT COUNT(*) from Factures`, function (err, count) {

            if (err) console.log(err)

            // send records as a response
            res.status(200).json({
              "datas": {
                "bills": bills.recordset,
                "count": count.recordset[0].number
              }
            });
            });
      });


  });
});

// Login
app.post('/login', (req, res) => {
  if (req.body.username === 'toto') {
    return res.send(true);
  } else {
    console.log(err);
  }

  var sql = require("mssql");

  // config for your database
  var config = {
      user: 'workUser',
      password: 'azert20.',
      server: 'localhost',
      database: 'Workshop'
  };

  // connect to your database
  sql.connect(config, function (err) {

      if (err) console.log(err);

      // create Request object
      var request = new sql.Request();

      // query to the database and get the records
      //request.query('select * from Factures', function (err, recordset) {
      request.query(`SELECT * FROM Utilisateurs\
                     WHERE U_utilisateur = ${req.body.username} AND U_motDePasse = ${req.body.password}`, function (err, user) {

          if (err) console.log(err)

          // send records as a response
          res.status(200).json(user);

      });


  });
});


// POST File
app.post('/bills/upload', upload.single('pdf'), (req, res) => {
  if (!req.file) {
    console.log("No file is available!");
    return res.send({
      success: false
    });

  } else {
    console.log('File is available!');
    return res.send({
      success: true
    })
  }
});

var server = app.listen(5000, function () {
    console.log('Server is running..');
});
