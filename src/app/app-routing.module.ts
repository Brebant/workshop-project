import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileUploadComponent } from './file-upload';
import { SynthesisComponent } from './synthesis/synthesis.component';
import { AuthGuard } from './services/auth-guard.service';
import { LoginComponent } from './components';

export const routes: Routes = [

  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'upload', component: FileUploadComponent, pathMatch: 'full' },
  { path: 'synthesis', component: SynthesisComponent, pathMatch: 'full' },

  // otherwise redirect to home
  {path: '**', redirectTo: 'login'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
