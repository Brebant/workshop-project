import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileUploader } from 'ng2-file-upload';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  constructor(private snackBar: MatSnackBar) { }

  files: any = [];
  URL = 'http://localhost:5000/bills/upload';


  public uploader: FileUploader = new FileUploader({
    url: this.URL,
    itemAlias: 'pdf'
  });

  ngOnInit(): void {
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onSuccessItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
      console.log('File successfully uploaded!');
    };
    this.uploader.onErrorItem = (item: any, status: any) => {
      console.log('Error File Details:', item);
      console.log('An error occured with this file!');
    };
    this.uploader.onCompleteAll = () => {
      this.openSnackBar('Files successfully upload !', 'close');
    };

  }

  openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top'
    });
  }

}
