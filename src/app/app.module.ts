import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material';
import { DragDrop } from './file-upload';
import { SynthesisComponent } from './synthesis/synthesis.component';
import { FooterComponent, HeaderComponent } from './components';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FileUploadModule } from 'ng2-file-upload';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



export const COMPONENTS = [
  AppComponent,
  FileUploadComponent,
  DragDrop,
  HeaderComponent,
  FooterComponent,
  SynthesisComponent,
  LoginComponent,
];

@NgModule({
  declarations: [
    COMPONENTS,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    FileUploadModule,
    BrowserAnimationsModule,
    MatSortModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
