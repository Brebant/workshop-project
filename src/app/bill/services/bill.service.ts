import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bill } from '../models';

@Injectable({
  providedIn: 'root'
})
export class BillService {

  constructor(private http: HttpClient) { }

  getBills(sort: string, order: string, page: any): Observable<any> {
    console.log(sort, order, page);
    const params = new HttpParams()
    .append('sort', sort)
    .append('order', order)
    .append('page', page + 1);
    return this.http.get<any>('http://localhost:5000/bills', { params });
  }

  getBills2(): Observable<Bill[]> {
    return this.http.get<Bill[]>('http://localhost:5000/bills');
  }

}
