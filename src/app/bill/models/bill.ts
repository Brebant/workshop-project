export interface Bill {
  number: number;
  firstname: string;
  lastname: string;
  date: Date;
  price: number;
}
