import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    const params = new HttpParams()
      .append('username', username)
      .append('password', password);
    return this.http.post<any>('http://localhost:5000/login', params);
  }
}
