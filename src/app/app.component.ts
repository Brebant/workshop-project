import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoginComponent } from './components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public router: Router){}
  title = 'workshop-app';
}
